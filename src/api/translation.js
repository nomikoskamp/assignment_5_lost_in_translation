import { createHeaders } from "./index"

const apiUrl = process.env.REACT_APP_API_URL

// Adds translation to the user at the api
export const addTranslation = async (user, translation) => {
    try {
        const response = await fetch(`${apiUrl}/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [...user.translations, translation]
            })
        })

        if (!response.ok) {
            throw new Error('Could not update the translation')
        }

        const result = await response.json()
        return [null, result]
    } catch (error) {
        return [error.message, null]
    }

}

// Clears all the translations of a user by id at the api
export const translationClearHistory = async (userId) => {
    try {
        const response = await fetch(`${apiUrl}/${userId}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: []
            })
        })

        if (!response.ok) {
            throw new Error('Could not delete the translation history')
        }
        const result = await response.json()
        
        return [null, result]
    } catch (error) {
        return [error.message, null]
    }

}