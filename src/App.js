import './App.css';
import './TextFieldStyle.css';
import './InputFieldStyles.css'
import {
  BrowserRouter,
  Routes,
  Route
} from 'react-router-dom'
import Login from './views/Login';
import Translation from './views/Translation';
import Profile from './views/Profile';

function App() {
  return (
    <BrowserRouter>
      <div className="App"></div>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/translation" element={<Translation />} />
        <Route path="/profile" element={<Profile />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
