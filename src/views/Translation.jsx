import PageNavigator from '../components/Navigator/PageNavigator'
import TranslationHeader from '../components/Translation/TranslationHeader'
import TranslationHistory from '../components/Translation/TranslationHistory'
import withAuth from '../hoc/withAuth'
import { useUser } from '../context/UserContext'
import { useForm } from 'react-hook-form'
import { useState } from 'react'

const Translation = () => {

    const [input, setInput] = useState('')
    const { register, handleSubmit, formState: { errors } } = useForm()
    const { user } = useUser()

    return (
        <>
            <PageNavigator username={user.username} />
            <TranslationHeader register={register} handleSubmit={handleSubmit} setInput={setInput}/>
            <TranslationHistory input={input} setInput={setInput} errors={errors} />
        </>
    )
}
export default withAuth(Translation) 