import LoginHeader from './../components/Login/LoginHeader'
import LoginForm from './../components/Login/LoginForm'
const Login = () => {
    return (
        <>
                <LoginHeader />
                <LoginForm />
        </>
    )
}
export default Login