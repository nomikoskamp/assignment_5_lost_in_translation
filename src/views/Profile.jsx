import PageNavigator from '../components/Navigator/PageNavigator'
import ProfileHeader from '../components/Profile/ProfileHeader'
import ProfileTranslationHistory from '../components/Profile/ProfileTranslationHistory'
import withAuth from '../hoc/withAuth'
import { useUser } from '../context/UserContext'

const Profile = () => {

    const { user } = useUser()
    
    return (
        <>
            <PageNavigator username={user.username} />
            <ProfileHeader />
            <ProfileTranslationHistory/>
        </>
    )
}
export default withAuth(Profile) 