import { translationClearHistory } from "../../api/translation"
import { STORAGE_KEY_USER } from '../../const/storageKeys'
import { storageSave } from "../../utils/storage"
import { useUser } from "../../context/UserContext"

const ProfileButton = () => {
    // Hooks
    const { user, setUser } = useUser()

    // Event Handler
    const onClick = async () => {
        if (!(user.translations.length > 0))                            // There are no translations
            return                                                      // do nothing

        if (!window.confirm('Are you sure? This cannot be undone.'))
            return

        const [clearError] = await translationClearHistory(user.id)     // Clears the translations in the api

        if (clearError !== null)
            return

        const updateUser = {
            ...user,
            translations: []
        }

        storageSave(STORAGE_KEY_USER, updateUser)                       // Clears the translations of the session storage
        setUser(updateUser)                                             // sets data to the current user
        
    }
    return (
        <>
            <button onClick={onClick} className="Button-bottom" type="submit" >
                <p>Clear history</p>
            </button></>
    )
}
export default ProfileButton