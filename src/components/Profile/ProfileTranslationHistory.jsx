import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"
import ProfileButton from "./ProfileButton"
import { useUser } from "../../context/UserContext"
import ProfileDeleteAccount from './ProfileDeleteAccount'
import './ProfileTranslationHistory.css'

const ProfileTranslationHistory = () => {

    const { user } = useUser()
    const firstToShow = (user.translations.length > 10) ? (user.translations.length - 10) : 0 // Checks the number of translations assigns 0 or length-10
    const translationList = user.translations
        .slice(firstToShow, (firstToShow + 11))                                               // last 10 translations
        .map((translation, index) =>                                                          // mapping each translation to a list component  
            <ProfileTranslationHistoryItem key={index + '-' + translation} translation={translation} />)

    return (
        <div className='Text-field-box'>
            <section>
                <ul>{translationList}</ul>
            </section>
            <span className='Bottom-line'>
                <ProfileButton />
                <ProfileDeleteAccount />
            </span>
        </div>
    )
}
export default ProfileTranslationHistory