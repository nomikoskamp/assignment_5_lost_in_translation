const ProfileTranslationHistoryItem = ({ translation }) => {

    return (
        <>
            <li className="Translation-li">{translation}</li>
        </>
    )
}
export default ProfileTranslationHistoryItem