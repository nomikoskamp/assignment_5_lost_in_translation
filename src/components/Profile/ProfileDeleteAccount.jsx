import { STORAGE_KEY_USER } from '../../const/storageKeys'
import { storageDelete } from "../../utils/storage"
import { useUser } from "../../context/UserContext"
import { deleteUser } from "../../api/user"

const ProfileDeleteAccount = () => {
    // Hooks
    const { user, setUser } = useUser()

    // Event handler
    const onClick = async () => {

        if (!window.confirm('Do you really want to delete your account?'))
            return

        storageDelete(STORAGE_KEY_USER)                             // Clears the session storage
        setUser(null)                                               // Sets user to null
        const [deleteError] = await deleteUser(user.id)             // Deletes user from api
        if (deleteError !== null)
             return window.alert('User could not delete.')
        
    }
    return (
        <>
            <button onClick={onClick} className="Button-bottom" type="submit" >
                <p>Delete account</p>
            </button></>
    )
}
export default ProfileDeleteAccount