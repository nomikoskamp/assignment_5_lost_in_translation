import TranslationForm from './TranslationForm'

const TranslationHeader = ({ register, handleSubmit, setInput }) => {

    return (
        <>
            <header className="Page-header">
                {/* <div className='Login-header-text'> */}
                    <TranslationForm register={register} handleSubmit={handleSubmit} setInput={setInput}/>
                {/* </div> */}
            </header>

        </>
    )
}
export default TranslationHeader