const TranslationHistory = ({ input, setInput, errors }) => {

    // Event handler
    const onSubmitClear = () => {
        setInput('')
    }

    // Render Functions
    const errorMessage = (() => {
        if (!errors.translations)
            return null

        if (errors.translations.type === 'required')                    // At least one char
            return <span>At least one character is required!</span>

        if (errors.translations.type === 'maxLength')                   // max chars limit is 40
            return <span>Too many characters! (max limit: 40)</span>
    })();

    return (
        <>
            <div className='Text-field-box'>
                <p>{errorMessage}</p>
                {input}
                <div className='Bottom-line'>
                    <button onClick={onSubmitClear} className="Button-bottom" type="submit" >
                        <p>Clear All</p>
                    </button>
                </div>
            </div>
        </>
    )
}
export default TranslationHistory