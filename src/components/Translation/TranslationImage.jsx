const TranslationImage = ({ character, key }) => {
    return<img className='Translated-characters' key={key + '-' + character}
        src={'/img/translation/' + character + '.png'} alt={'letter ' + character} width="70" />
 
}
export default TranslationImage