import { addTranslation } from '../../api/translation'
import { useUser } from '../../context/UserContext'
import { storageSave } from '../../utils/storage'
import { STORAGE_KEY_USER } from '../../const/storageKeys'
import TranslationImage from './TranslationImage'
import './TranslationForm.css'

const inputConfig = {                                // Input validation properties
    required: true,
    maxLength: 40,
}

const TranslationForm = ({ register, handleSubmit, setInput }) => {
    // Hooks
    const { user, setUser } = useUser()

    // Event handler
    const onSubmit = async (event) => {

        const characters = event.translations.replace(/[^a-zA-Z]+/g, '').toLowerCase().split('') // Ignores special chars, spaces and numbers  
                                                                                                 // splits sentence into an array of chars
        const charListItem = []

        for (let i = 0; i < characters.length; i++) {
            charListItem.push(<TranslationImage character={characters[i]} key={i}/>)             // Creates a new img component regarding the letter
        }
        setInput(charListItem)                                                                   // Adds them into an array of imgs

        const [error, updatedUser] = await addTranslation(user, event.translations)              // Saves the translation at the api
        if (error !== null) {
            return
        }

        storageSave(STORAGE_KEY_USER, updatedUser)   // Saves user data to the session storage (Keep UI state and Server state in sync)
        setUser(updatedUser)                         // Sets user to the updated user (Update context state)
    }

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)} className='TranslationForm-box'>
                <div className='Field'>
                <img src='img/graphics/keyboard-solid.svg' className='Keyboard' alt='keyboard'/>
                    <input className='Field-input'
                        type="text"
                        placeholder="Hello!"
                        {...register("translations", inputConfig)} />
                    <button className="Button-submit" type="submit" >
                        <img src='img/graphics/arrow-right-solid.svg' className="Arrow" alt="arrow" />
                    </button>
                </div>
            </form>
        </>
    )
}
export default TranslationForm