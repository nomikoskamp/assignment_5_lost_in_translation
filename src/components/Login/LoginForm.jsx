import './LoginForm.css'
import { useForm } from 'react-hook-form'
import { loginUser } from '../../api/user'
import { useNavigate} from 'react-router-dom'
import { useState, useEffect } from 'react'
import { storageSave } from '../../utils/storage'
import { useUser } from '../../context/UserContext'
import { STORAGE_KEY_USER } from '../../const/storageKeys'

const usernameConfig = {                    // Input validation properties
    required: true,
    minLength: 3,
    maxLength: 12
}

const LoginForm = () => {
    // Hooks
    const { register, handleSubmit, formState: { errors } } = useForm()
    const { user, setUser} = useUser()
    const navigate = useNavigate()
    
    // Local State
    const [ apiError, setApiError] = useState(null)

    // Side Effects
    useEffect(() => {
        if(user !== null){                  // If there is a user (created new or already existed)
            navigate('/translation')        // navigates to the Translation page
        }
    }, [user, navigate])

    // Event Handlers
    const onSubmit = async ({ username }) => {
        const [error, userResponse] = await loginUser(username)
        if(error !== null){
            setApiError(error)
        }
        if(userResponse !== null){          // If there is not an error and user logged in
            storageSave(STORAGE_KEY_USER, userResponse) // saves user's data to the session storage
            setUser(userResponse)           // sets user's data with the logged in user's data
        }
    }

    // Render Functions
    const errorMessage = (() => {           // Validation check of the input value of
        if (!errors.username)               // username's input filed
            return null

        if (errors.username.type === 'required')        // At east one char
            return <span>Username is required</span>

        if (errors.username.type === 'minLength')       // min 3 chars
            return <span>Username is too short</span>

        if (errors.username.type === 'maxLength')       // max 12 chars
            return <span>Username is too big</span>
    })();

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)} className="LoginForm-box">
                <fieldset className='Field'>
                    <img src='img/graphics/keyboard-solid.svg' className='Keyboard' alt='keyboard'/>
                    <input className='Field-input'
                        type="text"
                        placeholder="What's your name?"
                        {...register("username", usernameConfig)} />
                    <button className="Button-submit" type="submit" >
                        <img src='img/graphics/arrow-right-solid.svg' className="Arrow" alt="arrow" />
                    </button>
                </fieldset>
                <p className='Error-message'>{errorMessage} {apiError}</p>
                <span className='Bottom-line'></span>
            </form>
        </>
    )
}
export default LoginForm