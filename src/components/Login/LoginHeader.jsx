import './LoginHeader.css'
const LoginHeader = () => {
    return (
        <>
            <div className='Navigator'>
                <div className='Navigator-text'>
                    <h3>Lost in Translation</h3>
                </div>
                <div className='Bottom-line-nav'></div>
            </div>
            <header className="Login-header">
                <div className='Logo-box'>
                    <img src='img/logo/Logo.png' className="Logo" alt="logo" />
                </div>
                <div className='Login-header-text'>
                    <h1>Lost in Translation</h1>
                    <h2>Get started</h2>
                </div>
            </header>
        </>
    )
}
export default LoginHeader