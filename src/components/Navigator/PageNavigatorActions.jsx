import { useUser } from '../../context/UserContext'
import { STORAGE_KEY_USER } from '../../const/storageKeys'
import { storageDelete } from '../../utils/storage'
import { NavLink } from 'react-router-dom'
import './PagesNavigator.css'
import './NavigationBar.css'

const PageNavigatorActions = () => {
    // Hooks
    const { setUser } = useUser()

    // Event handlers
    const onLogout = () => {
        if (window.confirm('Are you sure?')) {
            storageDelete(STORAGE_KEY_USER)         // Deletes session storage data for the user
            setUser(null)                           // Setting user to null
        }
    }

    return (
        <>
            <li className='Navigation-li'>
                <NavLink className='Navigation-link' to='/translation'
                    style={({ isActive }) => ({
                        background: isActive ? '#845EC2' : 'none'
                    })}>
                    Translation
                </NavLink></li>
            <li className='Navigation-li'>
                <NavLink className='Navigation-link' to='/profile'
                    style={({ isActive }) => ({
                        background: isActive ? '#845EC2' : 'none'
                    })}>
                    Profile
                </NavLink></li>
            <li className='Navigation-li'>
                <NavLink onClick={onLogout} className='Navigation-link' to='/'
                    style={{ color: '#845EC2' }}>Log out</NavLink></li>
        </>
    )
}
export default PageNavigatorActions