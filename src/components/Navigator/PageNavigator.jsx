import PageNavigatorActions from './PageNavigatorActions'


const PageNavigator = ({ username }) => {

    return (
        <div className='Navigator'>
            <div className='Navigator-logo-box'>
                <div className='Logo-box'>
                    <img src='/img/logo/Logo.png' className="Logo" alt="logo" />
                </div>
            </div>
            <div className='Navigator-text'>
                <h3>Lost in Translation</h3>
            </div>
            <div className='Navigation-bar'>
                <ul className='Navigation-ul'>
                    <PageNavigatorActions />
                </ul>
            </div>
            <div className='User-box'>
                <div className='User-image-box'><img src='/img/graphics/circle-user-solid.svg' className="User-Image" alt="User" />
                </div>
                <div className='User-name'>{username}</div>
            </div>
            <div className='Bottom-line-nav'></div>
        </div>
    )
}
export default PageNavigator