# Assignment 2: Create a Sign Language Translator using React

## Table of Contents

- [Executive summary](#executive-summary)
- [Technologies](#technologies)
- [General Information](#general-information)
- [Component Tree](#component-tree)
- [Visit Page](#visit-page)
- [Maintainers](#maintainers)
- [License](#license)

## Executive summary

A Single Page Application of an online sign language translator, using React framework.

## Technologies

- [ ] Visual Studio Code
- [ ] React CRA (create-react-app)
- [ ] JavaScript, HTML, CSS
- [ ] NPM/Node.js (LTS – Long Term Support version)
- [ ] Browser Developer Tools for testing and debugging (React and Dev Tools)
- [ ] Figma
- [ ] Rest API: [noroff-assignment-api](https://github.com/dewald-els/noroff-assignment-api)


## General Information

This online web sign language translator has three pages, the [Login Page](#login-page), the [Translation Page](#translation-page) and the [Profile Page](#profile-page) with several actions in each page.

### Login Page

In this page there is an input field, where the user inserts a username and logging in to the app after pressing the purple button with the arrow.
If the username already exists, user is logging in to his account, if it does not exists a new profile is creating.

![alt text1](pics/login-page.PNG "Login Page")

### Translation Page

When the user is logged in, app navigates to the **Translation** page, where there is also an input field in where user could insert the desired word or sentence to be translated. Word is translated after pressing the purple button with the arrow and the translation is showed at the translation's text field. User could also clear the translation's field text by pressing the **Clear All** button at the bottom. 

![alt text1](pics/translation-page.PNG "Translation Page")

> **Only letter** characters are being translated. Special characters, numbers, spaces are ignored.


### Profile Page

When the user presses the **Profile** tab of navigation bar at the top of the page, app navigates to the **Profile** page. In this page, user could see the last 10 of the translated words or sentences. There are also two buttons at the bottom, **Clear History** button, that clears the history of translated words and the **Delete Account** button that deletes the account of the user and the app is navigated to the **Login** page.

![alt text1](pics/profile-page.PNG "Profile Page")

### Log out

Finally, there is a **Log out** button on top right at the navigation bar, that user logging out the profile and the app is navigated to the **Login** page.

![alt text1](pics/logout.PNG "Log out")

## Component Tree

Click to view the [Component Tree](component-tree.pdf) of the application.


## Visit Page

Click to visit [Lost in Translation](https://assignment-5-lost-in-translation.vercel.app) application.


## Maintainers

[Nomikos Kampourakis ](https://gitlab.com/nomikoskamp)
[Konstantinos Kokonos ](https://gitlab.com/konstantinoskokonos583)




## License

Nomikos Kampourakis
Konstantinos Kokonos

@ 2023